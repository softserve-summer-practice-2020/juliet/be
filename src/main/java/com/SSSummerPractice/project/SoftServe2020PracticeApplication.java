package com.SSSummerPractice.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoftServe2020PracticeApplication {

	public static void main(String[] args) {

		SpringApplication.run(SoftServe2020PracticeApplication.class, args);

	}
}

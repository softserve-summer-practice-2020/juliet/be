package com.SSSummerPractice.project.models.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Role {

    ROLE_ADMIN("ROLE_ADMIN"), ROLE_STUDENT("ROLE_STUDENT"),
    ROLE_MENTOR("ROLE_MENTOR");

    private String roleName;
}
